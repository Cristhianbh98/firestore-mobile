package facci.cristhianbacusoy.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class AuthActivity extends AppCompatActivity {

  private FirebaseAnalytics mFirebaseAnalytics;
  private Bundle bundle;
  private Integer GOOGLE_SIGN_IN = 100;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_auth);

    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    bundle = new Bundle();
    bundle.putString("message", "Integración de Firebase completa");

    mFirebaseAnalytics.logEvent("InitScrren", bundle);

    // Setup
    setup();
    session();
  }

  @Override
  protected void onStart() {
    super.onStart();
    LinearLayout authLayout = findViewById(R.id.authLayout);
    authLayout.setVisibility(View.VISIBLE);
  }

  private void session() {
    SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
    String email = prefs.getString("email", null);
    String provider = prefs.getString("provider", null);

    LinearLayout authLayout = findViewById(R.id.authLayout);

    if (email != null && provider != null) {
      authLayout.setVisibility(View.INVISIBLE);
      showHome(email, ProviderType.valueOf(provider));
    }
  }

  private void setup() {
    setTitle("Autentificación");

    Button singUpButton = (Button) findViewById(R.id.saveButton);
    EditText emailEditText = (EditText) findViewById(R.id.emailEditText);
    EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

    singUpButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!emailEditText.getText().toString().isEmpty() && !passwordEditText.getText().toString().isEmpty()) {
          FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString())
              .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                  if (task.isSuccessful()) {
                    showHome(task.getResult().getUser().getEmail().toString(), ProviderType.BASIC);
                  } else {
                    showAlert();
                  }
                }
              });
        }
      }
    });

    Button loginButton = (Button) findViewById(R.id.loginButton);

    loginButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!emailEditText.getText().toString().isEmpty() && !passwordEditText.getText().toString().isEmpty()) {
          FirebaseAuth.getInstance().signInWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString())
              .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                  if (task.isSuccessful()) {
                    showHome(task.getResult().getUser().getEmail().toString(), ProviderType.BASIC);
                  } else {
                    showAlert();
                  }
                }
              });
        }
      }
    });

    Button googleButton = (Button) findViewById(R.id.googleButton);

    googleButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        GoogleSignInOptions googleConf = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build();

        GoogleSignInClient googleClient = GoogleSignIn.getClient(AuthActivity.this, googleConf);

        Intent signInIntent = googleClient.getSignInIntent();
        googleClient.signOut();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
      }
    });
  }

  private void showAlert() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Error");
    builder.setMessage("Se ha producido un error autenticando el usuario");
    builder.setPositiveButton("Aceptar", null);
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  private void showHome(String email, ProviderType provider) {
    Intent homeIntent = new Intent(this, HomeActivity.class);
    homeIntent.putExtra("email", email);
    homeIntent.putExtra("provider", provider.name());

    startActivity(homeIntent);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == GOOGLE_SIGN_IN) {
      Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
      try {
        GoogleSignInAccount account = task.getResult(ApiException.class);

        if (account != null) {
          AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
          FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
              if (task.isSuccessful()) {
                showHome(account.getEmail(), ProviderType.GOOGLE);
              } else {
                showAlert();
              }
            }
          });

        }

      } catch (ApiException e) {
        showAlert();
      }
    }
  }
}