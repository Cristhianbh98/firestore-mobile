package facci.cristhianbacusoy.firebaseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

enum ProviderType {
  BASIC,
  GOOGLE
}

public class HomeActivity extends AppCompatActivity {
  private FirebaseFirestore db = FirebaseFirestore.getInstance();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);

    // SetUp
    Intent bundle = getIntent();

    String email = bundle.getStringExtra("email");
    String provider = bundle.getStringExtra("provider");

    setUp(email, provider);

    // Save data
    SharedPreferences.Editor prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit();
    prefs.putString("email", email);
    prefs.putString("provider", provider);
    prefs.apply();
  }

  private void setUp(String email, String provider) {
    setTitle("Inicio");
    TextView emailTextView = (TextView) findViewById(R.id.emailTextView);
    TextView providerTextView = (TextView) findViewById(R.id.providerTextView);

    emailTextView.setText(email);
    providerTextView.setText(provider);

    Button logoutButton = (Button) findViewById(R.id.logoutButton);

    logoutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SharedPreferences.Editor prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit();
        prefs.clear();
        prefs.apply();

        FirebaseAuth.getInstance().signOut();
        onBackPressed();
      }
    });

    Button saveButton = (Button) findViewById(R.id.saveButton);
    Button getButton = (Button) findViewById(R.id.getButton);
    Button deleteButton = (Button) findViewById(R.id.deleteButton);
    EditText adressEditText = findViewById(R.id.adressEditText);
    EditText phoneEditText = findViewById(R.id.phoneEditText);

    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Map<String, Object> user = new HashMap<>();
        user.put("adress", adressEditText.getText().toString());
        user.put("phone", phoneEditText.getText().toString());
        user.put("provider", provider);

        db.collection("users").document(email).set(user);
      }
    });

    getButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        db.collection("users").document(email).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
          @Override
          public void onSuccess(DocumentSnapshot documentSnapshot) {
            String adress = documentSnapshot.get("adress").toString();
            String phone = documentSnapshot.get("phone").toString();

            if (adress != "" && phone != "") {
              adressEditText.setText(adress);
              phoneEditText.setText(phone);
            }
          }
        });
      }
    });

    deleteButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        db.collection("users").document(email).delete();
        adressEditText.setText("");
        phoneEditText.setText("");
      }
    });
  }
}