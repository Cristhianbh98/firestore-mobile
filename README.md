# Repositorio para tarea de aplicaciones moviles 1

En esta aplicación se implemento firebase para poder realizar autentificación de usuario por email o por cuenta de google. 

También se ha implementado un pequeño CRUD donde el usuario puede guadar, editar y borrar su dirección y teléfono.

| Descripción | ¿Hecho? |
| ----------- | ----------- |
| Autentificación con Email |✅|
| Autentificación con Google Services |✅|
| CRUD con el documento de users |✅|

Hecho por &copy; Cristhian Bacusoy - 2022
